import { Injectable } from '@angular/core';
import { Event } from './event';

const EVENTS: Event[] = [
  { id: 1, name: 'Lili', location: 'L', startDate: '2017-06-21T13:00', endDate: '2017-09-21T13:00'},
  { id: 2, name: 'Mimi', location: 'M', startDate: '2017-07-21T13:00', endDate: '2017-11-21T13:00'}
];

@Injectable()
export class EventsService {

  constructor() { }

  getEvents(): Event[] {
    return EVENTS;
  }

  deleteEvent(event: Event): boolean {
    EVENTS.splice(EVENTS.indexOf(event), 1);
    return true;
  }
}
