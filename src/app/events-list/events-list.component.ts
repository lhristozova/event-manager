import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Event } from '../event';
import { EventsService } from '../events.service';

@Component({
  selector: 'app-events-list',
  templateUrl: './events-list.component.html',
  styleUrls: ['./events-list.component.scss']
})
export class EventsListComponent implements OnInit {
  events: Event[];

  @Output()
  onEventEdit: EventEmitter<Event> = new EventEmitter()

  constructor(private eventsService: EventsService) { }

  ngOnInit() {
    this.events = this.eventsService.getEvents();
  }

  onEventDelete(event: Event) {
    this.eventsService.deleteEvent(event);
  }
}
