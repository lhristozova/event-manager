export class Event {
  id: number;
  name: string;
  location: string;
  startDate: string;
  endDate: string;
}