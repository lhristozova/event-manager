import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Event } from '../event';
import { FormBuilder, FormControl, FormGroup, Validators  } from '@angular/forms';

@Component({
  selector: 'app-events-manage',
  templateUrl: './events-manage.component.html',
  styleUrls: ['./events-manage.component.scss']
})
export class EventsManageComponent implements OnInit {
  @Input()
  event: Event;

  eventsForm: FormGroup;

  @Output()
  onCancel: EventEmitter<undefined> = new EventEmitter()

  constructor(fb: FormBuilder) { 
     this.eventsForm = fb.group({
      name: ['', [Validators.required, Validators.minLength(2)]],
      location: ['', Validators.required],
      startDate: ['', Validators.required],
      endDate: ['', Validators.required]
    });

  }

  ngOnInit() {
    this.eventsForm.reset(this.event);
  }

  onSubmit() {
    if(this.eventsForm.valid) {
      console.log(this.eventsForm.value);
    }
    
  }
}
