import { Component } from '@angular/core';
import { Event } from './event';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  selectedEvent: Event;
}
